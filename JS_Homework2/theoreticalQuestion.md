Javascript function is a collection of statements. Functions can be either named or anonymous that can be called when required.
Functions is useful when tasks needs to be done repeatedly. Rather than writing same code over and over again, we can use functions to
reduce code we write.Some data can be required for executing code inside the function.We pass this data as arguments. Arguments give us
opportunity to manage amount of data passes to function. We can declare number of arguments that function needs and function won't
execute without getting this data. After executing code results can be returned via return keyword, but it is optional.
